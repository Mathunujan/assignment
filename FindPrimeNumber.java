import java.util.Scanner;

public class FindPrimeNumber {
	public static void demo() {
		Scanner sc = new Scanner(System.in);
		int m = -0;

		String cont = "";
		String cont2 = "";
		do {
			System.out.print("Enter the number- ");
			int x = sc.nextInt();
			int count = 0;

			for (int i = 1; i <= x; i++) {
				if (x % i == 0)
					count = count + 1;
			}

			if (count == 2)
				System.out.println(x + " is prime number");
			else
				System.out.println(x + " is not prime number");
		
			
			System.out.print("Do you want to continue? if yes press 'y' or press any key to abort- ");
			cont = sc.next();
			
			if (cont.equals("y")) {
				m = 1;
			} else 
			     System.out.println("Are you sure want to leave? if yes press 'y' or press 'n' to continue");
			cont2 = sc.next();
			if(cont2.equals("y"))
				System.out.println("end");
			
			
			}while (cont.equals("y") && m == 1 || cont2.equals("n"));
		}
	
	public static void demo2() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number-");
		int m = sc.nextInt();
		int t=0;
		for ( int x=1; x<=m; x++) {
			boolean cond = true;
			for ( int i=2; i <=x-1; i++) {
				
				if(x%i==0) {
					cond =false;
					break;
				}
				
			}
			
			if(cond==true) {
				t++;
				System.out.print(x+ " ");
				
			}
		}
		System.out.println("\n -------- Here " + t + " prime numbers in your number " + m+" --------" );
		
		
		}
	public static void main() {
		
		FindPrimeNumber method1 = new FindPrimeNumber();
		Scanner sc = new Scanner(System.in);
		String option2= "";
		do {
		System.out.println("__________________________________________________________________________ \n"
				         + "|Enter the key '1' to identify prime number                               |\n"                            
				         + "| or enter '2' for find prime numbers which are in your enter number      |\n"
				         + "|or enter 'o' to abort                                                    |\n"
				         + "|_________________________________________________________________________|\n"
				         + "Enter the key- ");
		int option = sc.nextInt();
		
		if(option!= 1 && option!= 2 && option !=0) {
			System.out.println("invalid key\n "
					+ "do u want to continue? if yes press 'y' or press 'n' to abort ");
			 option2 = sc.next();
			 if(option2.equals("n")) {
				 option=0;
		}}
		
		if(option==1) {
		method1.demo();
		}
		if(option==2) {
			method1.demo2();	
		}
		if(option==0) {
		System.out.println("Bye Bye User");
		}
		
		}while(option2.equals("y"));
		}
	
}



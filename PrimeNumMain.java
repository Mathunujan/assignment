import java.util.Scanner;

public class PrimeNumMain {
public static void main() {
	
	FindPrimeNumber method1 = new FindPrimeNumber();
	Scanner sc = new Scanner(System.in);
	String option2= "";
	do {
	System.out.println("__________________________________________________________________________ \n"
			         + "|Enter the key '1' to identify prime number                               |\n"                            
			         + "| or enter '2' for find prime numbers which are in your enter number      |\n"
			         + "|or enter 'o' to abort                                                    |\n"
			         + "|_________________________________________________________________________|\n"
			         + "Enter the key- ");
	int option = sc.nextInt();
	
	if(option!= 1 && option!= 2 && option !=0) {
		System.out.println("invalid key\n "
				+ "do u want to continue? if yes press 'y' or press 'n' to abort ");
		 option2 = sc.next();
		 if(option2.equals("n")) {
			 option=0;
	}}
	
	if(option==1) {
	method1.demo();
	}
	if(option==2) {
		method1.demo2();	
	}
	if(option==0) {
	System.out.println("Bye Bye User");
	}
	
	}while(option2.equals("y"));
	}
}
